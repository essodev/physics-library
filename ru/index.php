<?php
/**
 * (C) Sergii Shelestiuk, 2016
 */
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Электронный каталог библиотеки Физического факультета</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto" rel="stylesheet">

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/styles.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="app-layout">
<?php /*
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="/">
                Library
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li><a href="/">Home</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        Events <span class="caret"></span>
                    </a>
                    <!--
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('events.index') }}">Events</a></li>
                        <li><a href="{{ route('events.create') }}">Create New</a></li>
                        @if (empty($events) && !empty($event->id))
                        <li><a href="{!! route('events.edit', $event->id) !!}">Edit</a></li>
                        <li><a class="danger" href="#" data-toggle="modal" data-target="#confirmModal">Delete</a></li>
                        @endif
                    </ul>
                    //-->
                </li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                <li><a href="/login">Login</a></li>
                <li><a href="/register">Register</a></li>
                @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li><a href="/logout"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                    </ul>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
*/ ?>
<div class="container">

    <div class="row">
        <div class="form-wrapper col-sm-8 col-sm-offset-2 col-xs-12">
            <div class="title">
                <h2>Библиотека Физического факультета</h2>
                <h1>Электронный каталог</h1>
            </div>
            <div class="search-form">
                <form id="search-form" class="form">
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon input-group-addon-search">
                            <i class="fa fa-search"></i>
                        </span>
                        <input type="text" class="form-control input-lg" name="query" id="query" data-provide="typeahead" autocomplete="off" autofocus placeholder="Введите поисковый запрос...">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-search">Поиск</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-xs-12">
            <div id="results">
            </div>
        </div>
    </div>

</div>

<footer>
    <div class="footer-wrapper">
        <div class="footer">&copy; 2016 Веб-разработка &nbsp;- <a href="http://theory.phys.univ.kiev.ua/ru/staff/shelestiuk-sergey-nykolaevych">Сергей Шелестюк</a>, Виктор Задорожний</div>
    </div>
</footer>

<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script src="/js/script.js"></script>
</body>
</html>

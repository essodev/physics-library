<?php
/**
 * (C) Sergii Shelestiuk, 2016
 */
?>
<!DOCTYPE html>
<html lang="uk">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Оновлення бази даних | Електронний каталог бібліотеки Фізичного факультету</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto" rel="stylesheet">

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/styles.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="app-layout">
<div class="container">

    <div class="row">
        <div class="form-wrapper col-sm-6 col-sm-offset-3 col-xs-12">
            <div class="title">
                <h3>Оновлення бази даних</h3>
                <span>Оберіть файл бази даних MARC для імпорту</span>
            </div>

                <form class="form" method="POST" action="/api/v1/update" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="password" class="form-control" name="secret" id="secret" placeholder="Секретний ключ">
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="MAX_FILE_SIZE" value="10240000" />
                        <input type="file" class="form-control-file" name="dbfile" id="dbfile" aria-describedby="dbFileHelp">
                        <small id="dbFileHelp" class="form-text text-muted">Оберіть файл бази даних системи MARC для імпорту. <strong>Усі існуючі записи будуть видалені!</strong></small>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="form-control btn btn-search">Імпортувати</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="footer-wrapper">
        <div class="footer">&copy; 2016 Веб-розробка і впровадження&nbsp;- <a href="http://shelestiuk.com">Сергій Шелестюк</a></div>
    </div>
</footer>

<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</body>
</html>

<?php
/**
 * (C) Sergii Shelestiuk, 2016
 */

// Composer Class Autoloader
$loader = require_once __DIR__.'/../vendor/autoload.php';

$routes = [
    'api/v1/hints' => 'hints',
    'api/v1/search' => 'search',
    'api/v1/update' => 'update',
];

$database = new Database($GLOBALS['db_url']);
$controller = new Controller();
$controller->setDb($database);
$controller->setSecretHash($GLOBALS['secret_hash']);
$controller->setRoutes($routes);

$request = $controller->request();
$response = $controller->response($request);
print $response;

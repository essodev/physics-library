<?php
/**
 * (c) Sergii Shelestiuk, 2016
 */

$db_url = 'sqlite://base.sqlite';
$secret_hash = "SHA-256 CRC hash of your update secret key";

$GLOBALS['db_url'] = $db_url;
$GLOBALS['secret_hash'] = $secret_hash;

/**
 * (c) Sergii Shelestiuk, 2016
 */

$(document).ready(function() {

    $('#query').typeahead({
        items: 10,
        minLength: 3,
        hint: true,
        highlight: true,
        autoSelect: false,
        delay: 300,
        afterSelect: function(item) {
            ajaxSubmit(item);
        },
        source: function (query, process) {
            return $.getJSON('/api/v1/hints?q=' + query, function (data) {
                if (data.success === false) {
                    return false;
                }
                return process(data.result);
            })
        }
    });

    $('#search-form').on('submit', function(event) {
        event.preventDefault();
        var query = $('#query').val();
        ajaxSubmit(query);
    });

    /*
     $('a.autoquery').click(function() {
     //$('a.autoquery').on('click', function() {
     //$('#results li a').on('click', function(event) {

     alert("OK");
     //event.preventDefault();

     var query = $(this).text();
     console.log("query = " + query);

     $('#query').val(query);

     ajaxSubmit(query);
     });
     */

});

function ajaxSubmit(query) {

    if (typeof(query) === 'undefined')
        query = '';
    if (query.length == 0)
        return false;

    var data = {
        q: query
    };

    $.ajax({
        type: "GET",
        url: "/api/v1/search",
        data: data,
        dataType: "JSON",
        success: function(response) {
            if (response.success === false) {
                alert("Ошибка при отправке запроса: " + response);
                return false;
            }

            // FIXME: add circle loader
            //$('.contact-success').fadeIn().delay(4000).fadeOut();

            updateResults(response.result);
        }
    });

}

function updateResults(items) {
    if (items.length == 0) {
        $('#results').html('<div class="alert alert-info" role="alert">Не знайдено жодного запису.</div>');
        return;
    }
    $('#results').html('<ul class="list-group">');
    items.forEach(function(item, index) {
        var elem = '<li class="list-group-item">';
        if (item.author !== null) {
            elem += item.author + ' ';
        }
        elem += '<em>' + item.title + '</em>, ' + item.details + '</li>';
        $('#results').append(elem);
    });
    $('#results').append('</ul>');
}

# README #

## Library Online Catalog Search Engine ##

Built for [Physics faculty](http://www.phys.univ.kiev.ua/en/) of [National Taras Shevchenko University of Kyiv](http://univ.kiev.ua/en).

### Set-Up ###

* Create a database using the schema files *schema.*.sql*.
* Configure database connection and update secret key in *config.inc.php*.
* Run **composer install** to generate class autoloader.

### Contributors ###

* Sergii Shelestiuk
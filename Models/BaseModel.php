<?php
/**
 * (C) Sergii Shelestiuk, 2016
 */

class BaseModel
{
    private $db = null;
    private $database = null;
    private $charset = 'utf-8';

    var $id = null; // Mandatory model object field

    /**
     * BaseModel constructor.
     */
    public function __construct(Database $dbase)
    {
        $this->database = $dbase;
        $this->db = $this->database->getPDOInstance();
    }

    public function beginDbTransaction()
    {
        return $this->db->beginTransaction();
    }

    public function commitDbTransaction()
    {
        return $this->db->commit();
    }

    public function rollbackDbTransaction()
    {
        return $this->db->rollBack();
    }

    public function loadById($id = null)
    {
        if (empty($id) && empty($this->id))
            throw new Exception('Empty ID given to loadById');
        elseif (!empty($id))
            $this->id = $id;

        $db_table = strtolower(get_called_class()) . 's';

        $stmt = $this->db->prepare("SELECT * FROM $db_table WHERE id = :id");
        $stmt->bindParam(":id", $this->id);

        $result = $stmt->execute();
        if (!$result)
            return false;

        $obj = $stmt->fetchObject();
        if (empty($obj))
            return false;

        $this->import($obj);

        return $this;
    }

    public function search($value, $columns)
    {
        if (empty($value) || empty($columns))
            throw new Exception('Empty search request given');

        $db_table = strtolower(get_called_class()) . 's';

        $where_conditions = [];
        $order_conditions = [];
        for ($i = 0; $i < count($value); $i++) {
            $where_subconditions = [];
            foreach ($columns as $column) {
                $where_subconditions[] = "$column LIKE :$column$i";
            }
            $where_conditions[] = implode(' OR ', $where_subconditions);
        }
        foreach ($columns as $column)
            $order_conditions[] = "$column ASC";

        $sql = "SELECT * FROM $db_table WHERE (" . implode(') AND (', $where_conditions)
            . ") ORDER BY " . implode(', ', $order_conditions);
        $stmt = $this->db->prepare($sql);

        for ($i = 0; $i < count($value); $i++) {
            foreach ($columns as $column) {
                $val = is_array($value) ? $value[$i] : $value;
                $val = '%' . mb_strtolower($val, $this->charset) . '%';
                $stmt->bindValue(":$column$i", $val, PDO::PARAM_STR);
            }
        }

        $result = $stmt->execute();
        if (!$result)
            return false;

        $result_set = $stmt->fetchAll();

        // Convert result to a collection of items
        return $this->collection($result_set);
    }

    public function filter($columns)
    {
        if (!is_array($columns))
            throw new Exception('Incorrect filter format given');

        $db_table = strtolower(get_called_class()) . 's';

        $where_conditions = [];
        $order_conditions = [];
        foreach ($columns as $column => $value) {
            if (strpos($value, '%') !== false)
                $where_conditions[] = "$column LIKE :$column";
            else
                $where_conditions[] = "$column = :$column";

            $order_conditions[] = "$column ASC";
        }

        $sql = "SELECT * FROM $db_table WHERE " . implode(' AND ', $where_conditions)
            . " ORDER BY " . implode(', ', $order_conditions);
        $stmt = $this->db->prepare($sql);

        foreach ($columns as $column => $value)
            $stmt->bindValue(":$column", mb_strtolower($value, $this->charset), PDO::PARAM_STR);

        $result = $stmt->execute();
        if (!$result)
            return false;

        $result_set = $stmt->fetchAll();

        // Convert result array to a collection of items
        return $this->collection($result_set);
    }
    
    public function import($object)
    {
        // List of allowed class variables to import
        $allowed = array_keys(get_class_vars(get_called_class()));

        foreach (get_object_vars((object)$object) as $key => $value) {

            // Skip non-class properties
                if (!in_array($key, $allowed))
                continue;

            $this->$key = $value;
        }
    }

    public function getValuesByPartial($partial, $columns = null)
    {
        // List of class variables
        if (is_null($columns))
            $columns = array_keys(get_class_vars(get_called_class()));

        $result = [];
        foreach ($columns as $column) {
            $input_value = mb_strtolower($partial, $this->charset);
            $column_value = mb_strtolower($this->$column, $this->charset);
            if (strpos($column_value, $input_value) !== false)
                $result[] = $this->$column;
        }

        return $result;
    }

    public function collection(array $set) {

        $collection = new Collection();

        foreach ($set as $item) {
            $class = get_called_class();
            $obj = new $class($this->database);
            $obj->import($item);
            $collection->push($obj);
        }

        return $collection;
    }

    public function store($properties)
    {
        $db_table = strtolower(get_called_class()) . 's';

        if (empty($this->id)) {
            $names = implode(',', array_keys($properties));
            $values = ':' . implode(', :', array_keys($properties));
            $stmt = $this->db->prepare("INSERT INTO $db_table ($names) VALUES ($values)");
        }
        else {
            $name_values = '';
            foreach (array_keys($properties) as $property) {
                $name_values .= "$property = :$property, ";
            }
            $name_values = substr($name_values, 0, strlen($name_values) - 2);

            $stmt = $this->db->prepare("UPDATE $db_table SET $name_values WHERE id = :id");
            $stmt->bindParam(":id", $this->id);
        }

        foreach ($properties as $property => $value)
            $stmt->bindParam(":$property", $this->$property);

        $result = $stmt->execute();

        // Insert ID
        if ($result && empty($this->id))
            $this->id = $this->db->lastInsertId();

        $stmt->closeCursor();

        return $result;
    }

    public function drop()
    {
        $db_table = strtolower(get_called_class()) . 's';

        if (empty($this->id)) { // No ID set. Is not stored in a database
            return true;
        }

        // Delete record
        $stmt = $this->db->prepare("DELETE FROM $db_table WHERE id = :id");
        $stmt->bindParam(":id", $this->id);
        $result = $stmt->execute();

        // Reset ID
        if ($result)
            $this->id = null;

        $stmt->closeCursor();

        return $result;
    }

    public function dropAll()
    {
        $db_table = strtolower(get_called_class()) . 's';

        // Delete all records
        $result = $this->db->query("DELETE FROM $db_table");

        // Reset ID
        if ($result)
            $this->id = null;

        return $result;
    }
}

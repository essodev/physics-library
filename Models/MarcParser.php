<?php
/**
 * (C) Sergii Shelestiuk, 2016
 */

class MarcParser
{
    var $db = null;
    private $marc_file = null;
    protected $fh = null;
    protected $cursor = 0;

    /**
     * MarcParser constructor.
     * @param $marc_file - Working MARC file to set
     */
    public function __construct($marc_file = null)
    {
        $this->marc_file = $marc_file;
    }

    /**
     * @return $marc_file - current working MARC file.
     */
    public function getMarcFile()
    {
        return $this->marc_file;
    }

    /**
     * Sets working MARC file.
     *
     * @param $marc_file - working MARC file to set
     */
    public function setMarcFile($marc_file)
    {
        $this->marc_file = $marc_file;
    }

    public function openMarcFile()
    {
        if ($this->fh)
            return $this->fh;

        if (!$this->marc_file)
            return new Exception('Opening MARC file failed: No MARC filename set');

        $this->fh = gzopen($this->marc_file, 'rb');

        // Reset MARC file read cursor
        $this->resetMarcFile();

        return $this->fh;
    }

    public function closeMarcFile()
    {
        if ($this->fh) {
            gzclose($this->fh);
            $this->fh = null;
        }
    }

    /**
     * Resets MARC File Read Cursor to a Beginning of the DB File
     */
    public function resetMarcFile()
    {
        $this->cursor = 0;
    }

    /**
     * Returns next available parsed MARC record
     *
     * @return array $record
     */
    public function getNextRecord()
    {
        $marcFileRecord = $this->getNextMarcFileRecord();

        if ($marcFileRecord === false)
            return false;

        $marcRecord = $this->parseMarcFileRecord($marcFileRecord);
        return $marcRecord;
    }

    /**
     * Returns next available raw (binary) MARC file record.
     *
     * @return mixed $record
     */
    private function getNextMarcFileRecord()
    {
        $this->openMarcFile();

        if (gzseek($this->fh, $this->cursor) < 0 || feof($this->fh))
            return false;

        $raw_data = gzread($this->fh, 768);
        if ($raw_data === false)
            return false;

        $records = explode(chr(30).chr(29), $raw_data);
        $record = array_shift($records);
        $this->cursor += strlen($record) + 2;

        return $record;
    }

    /**
     * Changes input charset encoding from MARC CP866 to UTF-8
     *
     * @param $input - Input string or array
     * @return mixed
     */
    private function encConvert($input)
    {
        $toUTF8 = function ($str) {
            //$in_charset = 'cp866';
            $in_charset = 'cp1125';
            $out_charset = 'utf-8';
            return iconv($in_charset, $out_charset, $str);
        };

        if (is_array($input)) {
            return array_map($toUTF8, $input);
        }

        return $toUTF8($input);
    }

    /**
     * Parse raw (binary) file record to an associative array
     *
     * @param $marcFileRecord - binary MARC record to be parsed
     * @return array
     * @throws Exception
     */
    private function parseMarcFileRecord($marcFileRecord)
    {

        if ($marcFileRecord == false)
            return false;

        $author_signature = chr(31).'x'; // chr(31)xW(01)chr(31)

        $signature_pos = strpos($marcFileRecord, $author_signature);
        if ($signature_pos === false)
            throw new Exception('Error in MARC record format: No author signature found!');

        $record = new Publication($this->db);

        // The first field is a stripped author signature field: W...nn, here W is the first author/title(no author) letter
        // Sometimes nn numerical code is missing, followed by numerical e-type field and author field.
        $fields = explode(chr(31), substr($marcFileRecord, $signature_pos + strlen($author_signature)));

        // Extract author signature field (signature ending)
        $field = array_shift($fields);
        // Check if Author field is present (x?...nn)
        if (strlen($field) > 2) { // Full author signature present
            $author_flag = substr($field, -2, 2); // '00' or '01'
            if (is_numeric($author_flag) && $author_flag != 0)
                $record->author = $this->stripMarcRecordField(array_shift($fields));
            /*
            elseif (is_numeric($author_flag)) {
                // No author field present
                // Do nothing
            }
            else // e.g. xSch, e234234234.10, aSchaaf...
                echo "WARNING! Unknown author flag '$author_flag'";
            */
        }
        // TODO: Check if elseif->if was correct here
        // Extended author signature present (in the following field)
        if ($this->getMarcFieldType($fields[0]) == 'e') {
            $field = array_shift($fields);
            $author_flag = substr($field, -2, 2); // '00' or '01'
            if (is_numeric($author_flag) && $author_flag != 0)
                $record->author = $this->stripMarcRecordField(array_shift($fields));
            elseif (is_numeric($author_flag)) {
                // Some numeric codes in the extended author field.
                // Skip this field
            }
            else
                echo "FIXME: Check unknown extended author flag '$author_flag'";
        }

        // Event date, location and name
        $event_location_set = false; // Omit this field in the c-fields parsing
        foreach ($fields as $ind => $field) {
            if ($this->getMarcFieldType($field) == 'd') {
                $event_date = $this->stripMarcRecordField($field);
                if (!$this->isDZEndedMarcRecordField($field)) {
                    // Next field might be a c-type event location field
                    $ind++;
                    if (!isset($fields[$ind]) || $this->getMarcFieldType($fields[$ind]) != 'c') {
                        // The field does not exist or has incorrect type
                        continue;
                    }
                    $event_location = $fields[$ind];
                    $event_location_set = true;
                }
                $ind++;
                // Next field might be a a-type event name field
                if (!isset($fields[$ind]) || $this->getMarcFieldType($fields[$ind]) != 'a') {
                    // The field does not exist or has incorrect type
                    break;
                }
                $event_name = $fields[$ind];
                break;
            }
        }

        // Editor/year field (type 'c')
        $c_fields = $this->getMarcFieldsByType($fields, 'c');
        while ($c_field = array_shift($c_fields)) {
            if (is_numeric($this->stripMarcRecordField($c_field))) {
                $record->date = $this->stripMarcRecordField($c_field);
            }
            elseif (empty($record->editor)) {
                $record->editor = $this->stripMarcRecordField($c_field);
                if ($event_location_set) { // Event location instead of editor. Flush editor field
                    $record->editor = null;
                    $event_location_set = false;
                }
            }
            else {
                echo "FIXME: Unknown field of type 'c': " . $this->stripMarcRecordField($c_field);
            }
        }

        // Title
        $record->title = trim($this->stripMarcRecordField(array_shift($fields)));
        // Add event name, if any
        if (!empty($event_name))
            $record->title .= ' "' . $this->stripMarcRecordField($event_name) . '"';

        // Volume
        // Note: a few n-fields are possible (Session, Volume, etc)
        if ($n_fields = $this->getMarcFieldsByType($fields, 'n')) {
            // Remove consecutive duplicate volume field
            if ($n_fields[0] === $fields[0])
                array_shift($fields);

            $volume = $this->stripMarcRecordField($n_fields[0]);
            $volume = rtrim($volume, '.,');
            $record->title .= " " . $volume;
        }

        $fields = array_map(array(__CLASS__, 'stripMarcRecordField'), $fields);
        $record->details = implode(', ', $fields);
        $record->raw = $this->encConvert($marcFileRecord);

        // TODO: Polish title and authors => add spaces after ';:,', remove odd spaces, etc
        if (empty($record->title))
            echo "WARNING! Empty publication title";

        return $record;
    }


    private function getMarcFieldType($field)
    {
        if (isset($field[0]))
            return $field[0];

        return false;
    }

    /**
     * @param array $fields - Input field array.
     * @param string $fieldType - MARC field type to filter input fields by.
     * @return array $result
     */
    private function getMarcFieldsByType($fields, $fieldType)
    {
        $result = array();

        foreach ($fields as $field)
            if ($field[0] == $fieldType)
                $result[] = $field;

        return $result;
    }


    /**
     * Returns status of the field ending. True, if ended with a double zero signature, false otherwise
     *
     * @param $input - input MARC field
     * @return bool
     */
    private function isDZEndedMarcRecordField($input)
    {
        return (strlen($input) >= 3) && (strrpos($input, chr(30)."00", -3) !== false);
    }

    /**
     * Strips MARC record field from special characters.
     *
     * @param string $input - input MARC field
     * @return string $field - stripped input field string
     */
    private function stripMarcRecordField($input)
    {
        $len = strlen($input);
        $start = 0;
        if ($len && !is_numeric($input[0])) {
            $len -= 1;
            $start = 1;
        }

        // If record field ends with double zero signature
        if ($this->isDZEndedMarcRecordField($input))
            $len -= 3;

        $field = $this->encConvert(substr($input, $start, $len));
        return $field;
    }

    public function importMarcDB($db)
    {
        $this->db = $db;

        // TODO: Convert this heavy operation into consecutive batch jobs
        // Run the method as long as needed
        set_time_limit(600);
        ignore_user_abort();

        // Open MARC DB file and reset its read cursor to a beginning of the file
        $this->openMarcFile();

        // Temporary object for DB manipulations: DB URL setup and Lock wait time increase
        $publication = new Publication($this->db);

        $this->resetMarcFile();

        // Start DB Transaction and drop all records
        $publication->beginDbTransaction();
        $publication->dropAll();

        $count = 0;
        while ($record = $this->getNextRecord()) {
            $count++;

             if (!$record->save())
                throw new Exception("Error saving Publication record to a DB");
        }

        // Commit all changes to a database
        $publication->commitDbTransaction();

        // Return number of records imported
        return $count;
    }
}

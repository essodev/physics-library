<?php
/**
 * (C) Sergii Shelestiuk, 2016
 */

class Publication extends BaseModel
{
    //var $id = null;
    var $author = null;
    var $title = null;

    var $language = null;
    var $date = null; // publication date
    var $editor = null;
    var $pages = null;
    var $location = null;

    var $details = null; // All data except author, title and ID
    var $raw = null; // Raw MARC DB record


    public function save()
    {
        return $this->store(get_class_vars(__CLASS__));
    }

    public function load($id = null)
    {
        return $this->loadById($id, get_class_vars(__CLASS__));
    }

    public function getHints($string, $columns)
    {
        $collection = $this->search($string, $columns);

        $hints = [];
        foreach ($collection as $item) {
            if (($values = $item->getValuesByPartial($string, $columns)) && !in_array($values[0], $hints))
                $hints[] = $values[0];
        }

        return $hints;
    }
}

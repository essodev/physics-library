<?php

/**
 * (c) Sergii Shelestiuk, 2016
 */

class Collection implements Iterator
{
    private $items = array();

    public function __construct($array = null)
    {
        if (is_array($array)) {
            $this->items = $array;
        }
    }

    public function rewind()
    {
        reset($this->items);
    }

    public function current()
    {
        return current($this->items);
    }

    public function key()
    {
        $key = key($this->items);
        return $key;
    }

    public function next()
    {
        $next = next($this->items);
        return $next;
    }

    public function valid()
    {
        $key = $this->key();
        $valid = ($key !== null && $key !== false);
        return $valid;
    }

    /* @Not-iterative methods */

    public function first()
    {
        if (!isset($this->items[0]))
            return false;

        $first = $this->items[0];
        return $first;
    }

    public function last()
    {
        if (!isset($this->items[-1]))
            return false;

        $last = $this->items[-1];
        return $last;
    }

    public function push($item)
    {
        return array_push($this->items, $item);
    }

    public function count()
    {
        return count($this->items);
    }

    public function toArray()
    {
        return (array)$this->items;
    }
}

CREATE TABLE IF NOT EXISTS publications (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `author` VARCHAR(255) DEFAULT '',
  `title` VARCHAR(500) NOT NULL DEFAULT '',
  `language` VARCHAR(32) DEFAULT '',
  `date` date DEFAULT '0000-00-00',
  `editor` VARCHAR(200) DEFAULT '',
  `pages` INTEGER,
  `location` VARCHAR(128) DEFAULT '',
  `details` VARCHAR(500) DEFAULT '',
  `raw` TEXT -- MAX: ~900
) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=MyISAM;

CREATE FULLTEXT INDEX `author` ON publications (`author`);
CREATE FULLTEXT INDEX `title` ON publications (`title`);
CREATE FULLTEXT INDEX `details` ON publications (`details`);
CREATE INDEX `date` ON publications (`date`);
CREATE INDEX `location` ON publications (`location`);
-- CREATE UNIQUE INDEX `publication` ON publications (`author`, `editor`, `title`, `date`, `details`);

<?php
/**
 * (c) Sergii Shelestiuk, 2016
 *
 * Database connection class has been built using singleton pattern.
 * This pattern use is now discouraged. However, it brings code simplicity for the small projects like this.
 */

class Database
{

    // Variable to hold connection object
    protected static $db;

    /**
     * Database constructor.
     * Private constructor, because class cannot be instatiated externally
     */
    private function __construct() {

        // Instantiate the PDO object
        try {
            $pdo = $this->parseDbURL($this->getDB());

            if (!empty($pdo['db_charset']))
                $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES '". $pdo['db_charset'] . "'");
            else
                $options = null;

            // PDO Connection
            $this->db =  new PDO($pdo['dsn'], $pdo['db_user'], $pdo['db_pass'], $options);

            // PDO Error Mode
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            throw new Exception("DB connection error: " . $e->getMessage());
        }
        
        
        try {
            // Assign PDO object to db variable
            self::$db = new PDO( 'mysql:host=localhost;dbname=DBNAME', 'USERNAME', 'PASSWORD' );
            self::$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        }
        catch (PDOException $e) {
            //Output error - would normally log this to error file rather than output to user.
            echo "Connection Error: " . $e->getMessage();
        }

    }

    // Connection function. Static method - accessible without instantiation
    public static function getConnection() {

        // Guarantees single instance, if no connection object exists then create the one
        if (!self::$db) {
            // New connection object
            new Database();
        }

        // Return connection
        return self::$db;
    }
}

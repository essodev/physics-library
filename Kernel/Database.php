<?php
/**
 * (c) Sergii Shelestiuk, 2016
 */

class Database extends DBConnection
{

    /**
     * Database constructor.
     */
    public function __construct($db_url) {

        // Parse DB URL
        $pdo = $this->parseDbURL($db_url);

        // Create DB connection
        DBConnection::__construct($pdo);

        // When we construct a database handle, we assign an instance to the instance property
        $this->instantiate();
    }

    public function instantiate() {
        $this->instance = $this->connect();
    }

    public function getPDOInstance() {
        return $this->instance;
    }

    // DB URL: db_type://user:password@hostname:port/db_name
    // sqlite DB URL: sqlite:///path/to/db/file.sqlite
    private function parseDbURL($db_url)
    {
        $parts = @parse_url($db_url);

        if ($parts === false) {
            throw new Exception("DB Error: Could not parse DB settings");
        }

        $db_type = $parts['scheme'];
        $db_host = $parts['host'];
        $db_charset = 'utf8'; // TODO: Hardcoded charset

        $pdo = array(
            'dsn' => null,
            'db_user' => !empty($parts['user']) ? $parts['user'] : null,
            'db_pass' => !empty($parts['pass']) ? $parts['pass'] : null,
            'db_charset' => $db_charset,
            'db_options' => null, // PDO Options array
        );

        if ($db_type == 'sqlite') {

            $db_file = $db_host;
            if (!empty($parts['path']))
                $db_file = '/' . $db_host . $parts['path'];

            $dsn = "{$db_type}:$db_file";
        }
        else {
            $db_name = substr($parts['path'], 1);
            $dsn = "{$db_type}:host={$db_host};dbname={$db_name}" . (!empty($db_charset) ? ";charset={$db_charset}" : '');
        }
        $pdo['dsn'] = $dsn;

        if (!empty($db_charset))
            $pdo['db_options'] = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES '$db_charset'");
        else
            $pdo['db_options'] = null;

        return $pdo;
    }

}

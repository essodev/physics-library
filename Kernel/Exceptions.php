<?php
/**
 * (c) Sergii Shelestiuk, 2016
 */

class NotFoundException extends Exception
{

}

class AccessDeniedException extends Exception
{

}

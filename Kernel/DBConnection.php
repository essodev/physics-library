<?php
/**
 * (c) Sergii Shelestiuk, 2016
 */

class DBConnection
{

    protected function connect() {

        try {
            // PDO Connection
            $dbh = new PDO($this->dsn, $this->db_user, $this->db_pass, $this->db_options);

            // PDO Error Mode
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            throw new Exception("DB Error #" . $e->getCode() . ": " . $e->getMessage());
        }

        return $dbh;
    }

    /**
     * Database constructor.
     */
    public function __construct($pdo)
    {
        // Assigns the connection parameters
        foreach ($pdo as $param => $value) {
            $this->{$param} = $value;
        }
    }
}

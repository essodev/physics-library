<?php
/**
 * (c) Sergii Shelestiuk, 2016
 */

class Request
{

    public function __construct()
    {
        $this->import_safe();
    }
    
    public function import_safe()
    {
         $request_type = INPUT_GET;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $REQUEST = &$_POST;
            $request_type = INPUT_POST;
        }
        else {
            $REQUEST = &$_GET;
        }

        foreach ($REQUEST as $var => $value)
            if (preg_match('/\w+/', $var))
                $this->$var = filter_input($request_type, $var, FILTER_SANITIZE_STRING);

        foreach ($_FILES as $name => $struct)
            if (preg_match('/\w+/', $name))
                $this->$name = (object)$struct;
    }
    
}

<?php
/**
 * (c) Sergii Shelestiuk, 2016
 */

class AuthController
{
    var $hash = "";

    public function authenticate($secret)
    {
        $test_hash = hash('sha256', $secret);
        if (empty($secret) || $this->hash != $test_hash)
            throw new AccessDeniedException("Incorrect secret key");

        return true;
    }
}

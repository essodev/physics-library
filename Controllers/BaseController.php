<?php

/**
 * (c) Sergii Shelestiuk, 2016
 */

class BaseController
{
    var $routes = [];

    /**
     * @param array $routes
     */
    public function setRoutes($routes)
    {
        $this->routes = $routes;
    }

    public function route()
    {
        $path = $_SERVER['REQUEST_URI'];

        foreach ($this->routes as $route => $method) {
            if ($pos = strpos($path, $route)) {
                return $method;
            }
        }

        // Route not found
        throw new NotFoundException("Path not found.");
    }

    /**
     * @return Request
     */
    public function request()
    {
        return new Request();
    }

    public function response(Request $request)
    {
        try {
            // Get Route
            $route = $this->route();

            // Call Route Controller Method
            $response = $this->$route($request);

            // Response Data
            $response = [
                'success' => true,
                'error' => false,
                'result' => $response,
            ];
        }
        catch (NotFoundException $e) {
            $this->error_http_404($e->getMessage());
        }
        catch (AccessDeniedException $e) {
            $this->error_http_403($e->getMessage());
        }
        catch (Exception $e) {
            $response = [
                'success' => false,
                'error' => true,
                'errmsg' => $e->getMessage(),
            ];
        }

        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        $json = json_encode($response) . "\n";

        return $json;
    }

    public function error_http_404($message)
    {
        $details = "";
        if ($message)
            $details = "<p><em>$message</em></p>";
        header('HTTP/1.0 404 Not Found');
        print <<<EOL
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Not Found!</title>
        </head>
        <body>
            <h1><b>Page not found!</b></h1><h2><b>Error 404</b></h2><hr \>
            <p>The requested URL could not be found.</p>
            $details
        </body>
        </html>
EOL;
        exit();
    }

    public function error_http_403($message)
    {
        $details = "";
        if ($message)
            $details = "<p><em>$message</em></p>";
        header('HTTP/1.0 403 Forbidden');
        print <<<EOL
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Forbidden!</title>
        </head>
        <body>
            <h1><b>Access forbidden!</b></h1><h2><b>Error 403</b></h2><hr \>
            <p>You are not authorized to access this page.</p>
            $details
        </body>
        </html>
EOL;
        exit();
    }
}

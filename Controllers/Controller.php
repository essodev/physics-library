<?php
/**
 * (c) Sergii Shelestiuk, 2016
 */

class Controller extends BaseController
{

    var $search_columns = ['author', 'title', 'details'];
    var $publication = null;
    var $hash = "";
    var $db = null;

    /**
     * @param string $hash
     */
    public function setSecretHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @param Database $db
     */
    public function setDb($db)
    {
        $this->db = $db;
    }

    private function init()
    {
        $this->publication = new Publication($this->db);
    }

    public function search(Request $request)
    {
        $this->init();

        // Each word is a separate search term
        $search_terms = preg_split('{[+\s]+}', $request->q);
        $collection = $this->publication->search($search_terms, $this->search_columns);

        return $collection->toArray();
    }

    public function hints(Request $request)
    {
        $this->init();

        $hints = $this->publication->getHints($request->q, $this->search_columns);

        return $hints;
    }

    public function update(Request $request)
    {
        if (!isset($request->dbfile->error))
            throw new Exception('Error uploading file');
        elseif ($request->dbfile->error != 0)
            throw new Exception('Error #' . $request->dbfile->error . ' uploading file');

        $auth = new AuthController();
        $auth->hash = $this->hash;
        $auth->authenticate($request->secret);

        $parser = new MarcParser($request->dbfile->tmp_name);
        $imported_records = $parser->importMarcDB($this->db);

        return $imported_records;
    }
}
